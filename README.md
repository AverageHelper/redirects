# redirects

A basic Caddy config for renaming domains, without going through Cloudflare's own Redirect Rules platform.

## Goals

- Redirect domains without vendor lock-in to a proxy provider like Cloudflare.
- Optionally redirect subdomains cleanly (e.g. `git.avg.name` ~> `git.average.name`)
- Work with Cloudflare proxy if needed.

<!-- ## Usage

Serving wildcard subdomains requires getting an SSL certificate via the [DNS challenge](https://caddyserver.com/docs/automatic-https#dns-challenge) method, instead of the normal HTTP method. This requires [installing the appropriate DNS provider module](https://caddy.community/t/how-to-use-dns-provider-modules-in-caddy-2/8148), the [Namecheap DNS module](https://github.com/caddy-dns/namecheap), in this case.

Our one wildcard redirect case (so far) uses Namecheap for DNS. To install it, you'll first need to [build and install `xcaddy`](https://caddyserver.com/docs/build#xcaddy).

### 1. Build Caddy with DNS

Once you have that, run the following to build `caddy` from source, including the `namecheap` DNS module:

```sh
xcaddy build --with github.com/caddy-dns/namecheap
```

### 2. Install custom Caddy

There should now exist a new `caddy` binary in the working directory. Next, [move your new `caddy` binary in place](https://caddyserver.com/docs/build#package-support-files-for-custom-builds-for-debianubunturaspbian) like so:

```sh
sudo dpkg-divert --divert /usr/bin/caddy.default --rename /usr/bin/caddy
sudo mv ./caddy /usr/bin/caddy.custom
sudo update-alternatives --install /usr/bin/caddy caddy /usr/bin/caddy.default 10
sudo update-alternatives --install /usr/bin/caddy caddy /usr/bin/caddy.custom 50
sudo systemctl restart caddy
```

This will symlink your new `caddy` binary to `/usr/bin/caddy`, so don't move that file!

### 3. Add environment variables

Then you must add certain environment values (the API Key and user) [from Namecheap](https://www.namecheap.com/support/api/intro/) into the Caddy service file like so:

```sh
sudo systemctl edit caddy
```

Add the following to the resulting file:

```yml
[Service]
EnvironmentFile=/etc/caddy/.env
```

Then create `/etc/caddy/.env` with these contents (replacing your own values):

```sh
NAMECHEAP_API_KEY="<the API key>"
NAMECHEAP_API_USER="<your Namecheap username>"
CLIENT_IP="<your server's static IPv4 address>"
```

### 4. Start Caddy

Use systemd to run Caddy as a daemon:

```sh
sudo systemctl enable --now caddy
```

And you're done! -->

## Setup

After cloning, symlink this file to `/etc/caddy/Caddyfile`:

```sh
sudo ln -s /path/to/redirects/Caddyfile /etc/caddy/Caddyfile
```

This command will complain if `/etc/caddy/Caddyfile` already exists. Make a backup of that, or remove the file, then try again.

## Run Caddy

To use systemd to start Caddy as a system service, run the following:

```sh
sudo systemctl enable --now caddy
```

## Environment Variables

Some environment variables may be set to tweak Caddy's behavior. NOTE: these values will be naively wrapped with single-quotes before the Caddyfile is parsed (e.g. `"foo|bar baz` ~> `""foo|bar baz"`), so ensure your regex string escapes those properly.

- `BAD_BOTS_UA_REGEX`: A regex string that matches `User-Agent` strings whose sender should not have normal access to certain endpoints.

If you're running Caddy as a systemd service, see [these instructions](https://caddyserver.com/docs/running#overrides) for setting service overrides to define your environment variables.

## Updating the Caddyfile

After making changes to the Caddyfile (whether manually or using `git` commands), restart the Caddy service:

```sh
sudo systemctl reload caddy
```

## Logging

To access logs from the Caddy systemd service:

```sh
sudo journalctl -u caddy --no-pager | less +G
```
